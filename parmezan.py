# Generated with SMOP  0.41
from libsmop import *
# knn_tspi.m

    # SIMILARITY-BASED TIME SERIES PREDICTION
# Antonio Rafael Sabino Parmezan
# Advisor: Gustavo E. A. P. A. Batista
# LABIC / ICMC-USP
# 2018
    
    #--------------------------------------------------------------------------
# k-Nearest Neighbor - Time Series Prediction with Invariances (kNN-TSPI)
#--------------------------------------------------------------------------
    
@function
def knn_tspi(file=None,pred_window=None,k=None,len_query=None,*args,**kwargs):
    varargin = knn_tspi.varargin
    nargin = knn_tspi.nargin

    data=import_data(file)
# knn_tspi.m:11
    len_data=length(data)
# knn_tspi.m:12
    test_data=data(arange(len_data - pred_window + 1,len_data))
# knn_tspi.m:14
    predicted_data=zeros(pred_window,1)
# knn_tspi.m:15
    for i in arange(1,pred_window).reshape(-1):
        # Prediction without updating
        # if (i == 1)
        #    training_data = data(1 : len_data - pred_window);
        # else
        #    training_data = [training_data; predicted_data(i - 1)];
        # end
        training_data=data(arange(1,len_data - pred_window + i - 1))
# knn_tspi.m:25
        predicted_data[i]=predict(training_data,k,len_query)
# knn_tspi.m:28
        # last_value = training_data(length(training_data));
        # predicted_data(i) = last_value + predict(training_data, k, len_query);
    
    
    # disp([test_data, predicted_data]);
    # plot(1:length(test_data), test_data, 1:length(predicted_data), predicted_data);
    # legend('actual', 'predicted');
    
    error=error_measure(test_data,predicted_data,data(len_data - pred_window))
# knn_tspi.m:39
    return error
    
if __name__ == '__main__':
    pass
    
    #--------------------------------------------------------------------------
    
@function
def predict(data=None,k=None,len_query=None,*args,**kwargs):
    varargin = predict.varargin
    nargin = predict.nargin

    len_data=length(data)
# knn_tspi.m:44
    indexes,min_dists=similarity_search(data,k,len_query,nargout=2)
# knn_tspi.m:46
    min_k=sum(mod(- indexes,indexes + 1))
# knn_tspi.m:47
    
    predictions=zeros(min_k,1)
# knn_tspi.m:48
    query=data(arange(len_data - len_query + 1,len_data))
# knn_tspi.m:50
    query_mean=mean(query)
# knn_tspi.m:51
    query_std=std(query)
# knn_tspi.m:52
    for i in arange(1,min_k).reshape(-1):
        subseq=data(arange(indexes(i),indexes(i) + len_query))
# knn_tspi.m:55
        subseq=z_score2(subseq,query)
# knn_tspi.m:56
        subseq=dot(subseq,query_std) + query_mean
# knn_tspi.m:57
        # MVR
        # predictions(i) = subseq(len_query + 1) - subseq(len_query);
        # MVA or WA
        predictions[i]=subseq(len_query + 1)
# knn_tspi.m:63
    
    
    # prediction = median(predictions);
    prediction=mean(predictions)
# knn_tspi.m:67
    
    return prediction
    
if __name__ == '__main__':
    pass
    
    #--------------------------------------------------------------------------
    
@function
def similarity_search(data=None,k=None,len_query=None,*args,**kwargs):
    varargin = similarity_search.varargin
    nargin = similarity_search.nargin

    len_data=length(data)
# knn_tspi.m:73
    query=data(arange(len_data - len_query + 1,len_data))
# knn_tspi.m:74
    query=z_score1(query)
# knn_tspi.m:75
    
    
    min_dists=dot(ones(k,1),realmax)
# knn_tspi.m:77
    indexes=zeros(k,1)
# knn_tspi.m:78
    for i in arange(1,k).reshape(-1):
        for j in arange(1,len_data - (dot(2,len_query)) - 1).reshape(-1):
            if (logical_not(trivial_match(j,indexes,i,len_query))):
                subseq=data(arange(j,j + len_query - 1))
# knn_tspi.m:83
                subseq=z_score1(subseq)
# knn_tspi.m:84
                d=distance(subseq,query)
# knn_tspi.m:85
                if (isnan(d) or d == Inf):
                    warning(strcat('distance_measure=',num2str(d)))
                if (d < min_dists(i)):
                    min_dists[i]=d
# knn_tspi.m:92
                    indexes[i]=j
# knn_tspi.m:93
    
    return indexes,min_dists
    
if __name__ == '__main__':
    pass
    
    #--------------------------------------------------------------------------
    
@function
def weighted_average(min_dists=None,predictions=None,*args,**kwargs):
    varargin = weighted_average.varargin
    nargin = weighted_average.nargin

    len_pred=length(predictions)
# knn_tspi.m:102
    max_dist=max(min_dists)
# knn_tspi.m:103
    if (len_pred > 1 and max_dist != 0):
        min_dists=min_dists / max_dist
# knn_tspi.m:106
    
    
    alpha=0.5
# knn_tspi.m:109
    for i in arange(1,len_pred).reshape(-1):
        if (min_dists(i) == 0):
            min_dists[i]=1
# knn_tspi.m:113
        else:
            # min_dists(i) = 1 - min_dists(i);
            # min_dists(i) = 1 / min_dists(i);
            # min_dists(i) = 1 / (min_dists(i)^2);
            # min_dists(i) = exp(-min_dists(i)^2);
            # min_dists(i) = exp(-min_dists(i)^2 / (2 * alpha^2));
            # min_dists(i) = exp(-min_dists(i) / (2 * alpha^2));
            min_dists[i]=1 - exp(- min_dists(i) / (dot(2,alpha ** 2)))
# knn_tspi.m:121
    
    
    prediction=sum(multiply(min_dists,predictions)) / sum(min_dists)
# knn_tspi.m:125
    return prediction
    
if __name__ == '__main__':
    pass
    
    #--------------------------------------------------------------------------
    
@function
def z_score1(s=None,*args,**kwargs):
    varargin = z_score1.varargin
    nargin = z_score1.nargin

    if (std(s) == 0):
        z=s - mean(s)
# knn_tspi.m:131
    else:
        z=(s - mean(s)) / std(s)
# knn_tspi.m:133
    
    return z
    
if __name__ == '__main__':
    pass
    
    
@function
def z_score2(s=None,q=None,*args,**kwargs):
    varargin = z_score2.varargin
    nargin = z_score2.nargin

    s1=s(arange(1,end() - 1))
# knn_tspi.m:138
    if (var(s1) > var(q)):
        m=mean(s1)
# knn_tspi.m:141
        d=std(s1)
# knn_tspi.m:142
    else:
        m=mean(s)
# knn_tspi.m:144
        d=std(s)
# knn_tspi.m:145
    
    
    if (d == 0):
        z=s - m
# knn_tspi.m:149
    else:
        z=(s - m) / d
# knn_tspi.m:151
    
    return z
    
if __name__ == '__main__':
    pass
    
    #--------------------------------------------------------------------------
    
@function
def trivial_match(pos=None,indexes=None,inc=None,len_query=None,*args,**kwargs):
    varargin = trivial_match.varargin
    nargin = trivial_match.nargin

    tm=copy(false)
# knn_tspi.m:157
    for i in arange(1,inc - 1).reshape(-1):
        #if (abs(pos - indexes(i)) <= len_query)
        if (abs(pos - indexes(i)) <= len_query):
            tm=copy(true)
# knn_tspi.m:161
            break
    
    return tm
    
if __name__ == '__main__':
    pass
    
    #--------------------------------------------------------------------------
    
@function
def ED(s=None,t=None,*args,**kwargs):
    varargin = ED.varargin
    nargin = ED.nargin

    d=sqrt(sum((s - t) ** 2))
# knn_tspi.m:169
    return d
    
if __name__ == '__main__':
    pass
    
    
@function
def CID(Q=None,C=None,*args,**kwargs):
    varargin = CID.varargin
    nargin = CID.nargin

    CE_Q=sqrt(sum(diff(Q) ** 2)) + eps
# knn_tspi.m:173
    CE_C=sqrt(sum(diff(C) ** 2)) + eps
# knn_tspi.m:174
    d=dot(ED(Q,C),(max(CE_Q,CE_C) / min(CE_Q,CE_C)))
# knn_tspi.m:176
    return d
    
if __name__ == '__main__':
    pass
    
    
@function
def distance(s=None,t=None,*args,**kwargs):
    varargin = distance.varargin
    nargin = distance.nargin

    d=CID(s,t)
# knn_tspi.m:180
    return d
    
if __name__ == '__main__':
    pass
    
