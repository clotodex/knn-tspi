import pandas as pd
import os
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib import ticker

INCLUDE_SIMPLE=True
TIME_KEY="FIN_MONTH"

data_knn = pd.read_csv("validation_experimentresults_ep.txt")
data_sarima = pd.read_csv("arima_experimentresults_ep.txt")
data_mean = pd.read_csv("mean_predictor_experimentresults_ep.txt")
data_last_point = pd.read_csv("lastpoint_predictor_experimentresults_ep.txt")

to_rename = {
    "rmse": "RMSE",
    "mae": "MAE",
    "mape"   : "MAPE"   ,
    "ep_mae" : "EP-MAE" ,
    "ep_rmse": "EP-RMSE",
}
data_knn = data_knn.rename(columns=to_rename)
data_sarima = data_sarima.rename(columns=to_rename)
data_mean = data_mean.rename(columns=to_rename)
data_last_point = data_last_point.rename(columns=to_rename)

alldivs = ["A", "B", "C", "D", "E"]

sns.set(style="whitegrid")
sns.set_context("talk")
sns.set_palette("muted")

for div in alldivs:
    query_knn = data_knn.query("time=='{}' & div=='{}'".format(TIME_KEY, div))
    query_sarima = data_sarima.query("time=='{}' & div=='{}'".format(TIME_KEY, div))

    #f, ax = plt.subplots(1)
    plt.plot(query_knn.predwin, query_knn.MAE, label='{}-kNN'.format(div), color="orange")
    plt.plot(query_sarima.predwin, query_sarima.MAE, label='{}-SARIMA'.format(div), color="green")
    #ax.set_ylim(ymin=0)
    #ax.legend()
sns.despine()
plt.legend()
plt.title("Error Progression")
plt.show()


f, ax = plt.subplots(1, figsize=(23,10.8), dpi=100)
for div in alldivs:
    query_knn = data_knn.query("time=='{}' & div=='{}'".format(TIME_KEY, div))

    #f, ax = plt.subplots(1)
    ax.plot(query_knn.predwin, query_knn.RMSE, label='{}'.format(div))
    #ax.set_ylim(ymin=0)
    #ax.legend()
ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: ('%.1f')%(x/1e6)))
ax.set_ylabel("RMSE (Million €)")
ax.set_xlabel("Weeks" if TIME_KEY == "FIN_WEEK" else "Months")
sns.despine()
plt.legend()
plt.title("Error Progression per Division for kNN-TSPI")
f.savefig(os.path.join("plots","errorprogression_{}_ALL_alldivs.png".format(TIME_KEY)), bbox_inches='tight')
plt.show()

f, ax = plt.subplots(1, figsize=(23,10.8), dpi=100)
for div in alldivs:
    query_sarima = data_sarima.query("time=='{}' & div=='{}'".format(TIME_KEY, div))

    #f, ax = plt.subplots(1)
    ax.plot(query_sarima.predwin, query_sarima.RMSE, label='{}'.format(div))
    #ax.set_ylim(ymin=0)
    #ax.legend()
ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: ('%.1f')%(x/1e6)))
ax.set_ylabel("RMSE (Million €)")
ax.set_xlabel("Weeks" if TIME_KEY == "FIN_WEEK" else "Months")
sns.despine()
plt.legend()
plt.title("Error Progression per Division for SARIMA")
f.savefig(os.path.join("plots","errorprogression_{}_ALL_alldivs_sarima.png".format(TIME_KEY)), bbox_inches='tight')
plt.show()

# TODO bug
OLD_TIME_KEY = TIME_KEY
TIME_KEY = "FIN_WEEK"

predwin = 24
# bar plot Error (€)
query_knn = data_knn.query("time=='{}' & predwin=='{}' & div=='ALL'".format(TIME_KEY, predwin))
bars_knn = pd.melt(query_knn[['MAE','RMSE']], var_name="Measure", value_name="Error (Million €)")
bars_knn["Method"] = "kNN-TSPI"
query_sarima = data_sarima.query("time=='{}' & predwin=='{}' & div=='ALL'".format(TIME_KEY, predwin))
bars_sarima = pd.melt(query_sarima[['MAE','RMSE']], var_name="Measure", value_name="Error (Million €)")
bars_sarima["Method"] = "SARIMA"
bars = pd.concat([bars_knn, bars_sarima])
ax = sns.barplot(x="Measure", y="Error (Million €)", hue="Method", data=bars)
ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: ('%.1f')%(x/1e6)))
plt.savefig(os.path.join("plots","bars_week_{}_ALL_abs.png".format(predwin)), bbox_inches='tight')
plt.show()


query_knn = data_knn.query("time=='{}' & predwin=='{}' & div=='ALL'".format(TIME_KEY,predwin))
bars_knn = pd.melt(query_knn[['MAPE','EP-MAE', 'EP-RMSE']], var_name="Measure", value_name="Error (%)")
bars_knn["Method"] = "kNN-TSPI"
query_sarima = data_sarima.query("time=='{}' & predwin=='{}' & div=='ALL'".format(TIME_KEY,predwin))
bars_sarima = pd.melt(query_sarima[['MAPE','EP-MAE', 'EP-RMSE']], var_name="Measure", value_name="Error (%)")
bars_sarima["Method"] = "SARIMA"
bars = pd.concat([bars_knn, bars_sarima])
ax = sns.barplot(x="Measure", y="Error (%)", hue="Method", data=bars)
plt.savefig(os.path.join("plots","bars_week_{}_ALL_per.png".format(predwin)), bbox_inches='tight')
plt.show()

TIME_KEY = OLD_TIME_KEY

#exit()

#for per_measure in all_measures[2:]:

query_knn = data_knn.query("time=='{}' & div=='ALL'".format(TIME_KEY))
query_sarima = data_sarima.query("time=='{}' & div=='ALL'".format(TIME_KEY))
query_mean = data_mean.query("time=='{}' & div=='ALL'".format(TIME_KEY))
query_last_point = data_last_point.query("time=='{}' & div=='ALL'".format(TIME_KEY))
print(query_knn)

all_measures = ["MAE", "RMSE", "MAPE", "EP-MAE", "EP-RMSE"]
for i,measure in enumerate(all_measures):
    plt.plot(query_knn.predwin, query_knn[measure], label="kNN-TSPI")
    plt.plot(query_sarima.predwin, query_sarima[measure], label="SARIMA")
    if INCLUDE_SIMPLE:
        plt.plot(query_mean.predwin, query_mean[measure], label="Mean")
        plt.plot(query_last_point.predwin, query_last_point[measure], label="Last point")
    if i<2:
        plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: ('%.1f')%(x/1e6)))
    plt.xlabel("Weeks" if TIME_KEY == "FIN_WEEK" else "Months")
    plt.ylabel("{} ({})".format(measure, "Million €" if i<2 else "%"))
    plt.title("Error Progression")
    sns.despine()
    plt.legend()
    plt.savefig(os.path.join("plots","errorprogression_{}{}_{}_ALL_{}.png".format("withsimple_" if INCLUDE_SIMPLE else "", TIME_KEY,predwin, measure)), bbox_inches='tight')
    plt.show()
