from knntspi import KnnTSPI
import numpy as np
import measures

if __name__== "__main__":

    def load_data():
        from dataloader import load_data
        keys_time, keys_divs, plotdata = load_data(structuralbreak=False)
        time, points = np.array(plotdata["FIN_MONTH-ALL"])
        return np.array(points, dtype=np.float64)

    def split_data(data, testcount=None):
        if testcount:
            return data[:len(data) - testcount], data[len(data) - testcount:]
        return data[:int(len(data)*0.7)], data[int(len(data)*0.7):]

    PREDICTIONS=9

    data = load_data()#[:-1]
    train,test = split_data(data, testcount=PREDICTIONS)

    print("train: ", train.shape)
    print("test: ", test.shape)

    model = KnnTSPI(k=4, l=3)
    pred = model.predict(train, PREDICTIONS)
    print("Prediction: ", pred.shape)
    
    print("MAPE: ", measures.MAPE(test, pred))
    print("MPE: ", measures.MPE(test, pred))
    print("RMSE: ", measures.RMSE(test, pred))
    print("MAE: ", measures.MAE(test, pred))
    tm = test.mean()
    print("EP-RMSE: ", measures.RMSE(test, pred)/tm)
    print("EP-MAE: ", measures.MAE(test, pred)/tm)
    
    #Compare with mean of train data:
    
    x = train.mean()
    print("Error measures for train mean: ")
    print(x)
    print("MAPE: ", measures.MAPE(test, x))
    print("MPE: ", measures.MPE(test, x))
    print("RMSE: ", measures.RMSE(test, x))
    print("MAE: ", measures.MAE(test, x))
    print("EP-RMSE: ", measures.RMSE(test, x)/tm)
    print("EP-MAE: ", measures.MAE(test, x)/tm)
    
    y = train[-1]
    print("Error measures for last point predictor: ")
    print(x)
    print("MAPE: ", measures.MAPE(test, y))
    print("MPE: ", measures.MPE(test, y))
    print("RMSE: ", measures.RMSE(test, y))
    print("MAE: ", measures.MAE(test, y))
    print("EP-RMSE: ", measures.RMSE(test, y)/tm)
    print("EP-MAE: ", measures.MAE(test, y)/tm)
    
    

    

    import matplotlib.pyplot as plt

    f, ax = plt.subplots(1)
    ax.plot(range(PREDICTIONS), test, label="actual")
    ax.plot(range(PREDICTIONS), pred, label="prediction")
    #ax.set_ylim(ymin=0)
    ax.legend()
    plt.title("RMSE")
    plt.show(f)
