import numpy as np
import matplotlib.pyplot as plt
from dataloader import load_data

keys_time, keys_divs, plotdata = load_data()

time, data = plotdata["FIN_MONTH-ALL"]
data = np.array(data)

FREQUENCY=12 # Try with different frequencies -> m automatically changes with frequency

from pmdarima.arima.utils import ndiffs
## Adf Test
print("adf: ",ndiffs(data, test='adf'))  # 2

# KPSS test
print("kpss: ", ndiffs(data, test='kpss'))  # 0

# PP test:
print("pp: ", ndiffs(data, test='pp'))  # 2

#train = data[:int(0.8*len(data))]
#test = data[int(0.8*len(data)):]

PREDICTIONS=9
train = data[:len(data)-PREDICTIONS]
test = data[len(data)-PREDICTIONS:]

print("train: #{}, test: #{}".format(len(train), len(test)))

from pyramid.arima.stationarity import ADFTest
adf_test = ADFTest(alpha=0.05)
print(adf_test.is_stationary(data))

from pyramid.arima import auto_arima
stepwise_model = auto_arima(train, start_p=1, start_q=1,
                           max_p=8, max_q=8, m=FREQUENCY,
                           start_P=0, start_Q=0, max_P=8, max_Q=8, seasonal=True,
                           d=None, D=None, trace=True,
                           error_action='warn',
                           suppress_warnings=True,
                           stepwise=True,random=False, random_state=20, n_fits=30)
#scoring=mse & scoring=mae possible
#If random=True : Random search
#If seasonal=False -> ARIMA (since no seasonal components
print("AIC: ",stepwise_model.aic())

print(stepwise_model.summary())

#stepwise_model.fit(train)

future_forecast = stepwise_model.predict(n_periods=len(test))

import measures
print("MAPE: ", measures.MAPE(test,future_forecast))
print("MSE: ", measures.MSE(test,future_forecast))
print("RMSE: ", measures.RMSE(test,future_forecast))
print("MAE: ", measures.MAE(test,future_forecast))

params = stepwise_model.get_params()
print("order: ", params['order'])
print("seasonal order: ", params['seasonal_order'])

import matplotlib.pyplot as plt

f, ax = plt.subplots(1)
ax.plot(range(PREDICTIONS), test, label="actual")
ax.plot(range(PREDICTIONS), future_forecast, label="prediction")
#ax.set_ylim(ymin=0)
ax.legend()
plt.title("MSE")
plt.show(f)
