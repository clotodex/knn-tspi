from dataloader import load_data
import numpy as np

if __name__== "__main__":
    def split_data(data, testcount=None):
        if testcount:
            return data[:len(data) - testcount], data[len(data) - testcount:]
        return data[:int(len(data)*0.7)], data[int(len(data)*0.7):]

    keys_time, keys_divs, plotdata = load_data()

    results=[]
    try:
        with open("arima_experimentresults.txt", "r") as myfile:
            next(myfile)
            for line in myfile:
                time,div,predwin,orderp,orderd,orderq,seasonalp,seasonald,seasonalq,m,mape,mae,rmse = line.rstrip().split(",")
                results.append((time,div,int(predwin),int(orderp),int(orderd),int(orderq),int(seasonalp),int(seasonald),int(seasonalq),int(m),float(mape),float(mae),float(rmse)))
    except IOError:
        exit()

    with open('arima_experimentresults_ep.txt', 'w') as file:
        file.write('time,div,predwin,orderp,orderd,orderq,seasonalp,seasonald,seasonalq,m,mape,mae,rmse,ep_mae,ep_rmse\n')
        for time,div,predwin,orderp,orderd,orderq,seasonalp,seasonald,seasonalq,m,mape,mae,rmse in results:
            t, data = plotdata["{}-{}".format(time,div)]
            data = np.array(data)
            train,test = split_data(data, testcount=predwin)
            mean = np.mean(test)
            ep_mae = mae/mean *100
            ep_rmse = rmse/mean *100
            result = "{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format (time,div,predwin,orderp,orderd,orderq,seasonalp,seasonald,seasonalq,m,mape,mae,rmse,ep_mae,ep_rmse)
            file.write(result)


