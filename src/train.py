from dataloader import load_data
from knntspi import KnnTSPI
import numpy as np

if __name__== "__main__":
    def split_data(data, testcount=None):
        if testcount:
            return data[:len(data) - testcount], data[len(data) - testcount:]
        return data[:int(len(data)*0.7)], data[int(len(data)*0.7):]

    keys_time, keys_divs, plotdata = load_data()

    val_time=["FIN_DAY", "FIN_WEEK", "FIN_MONTH", "FIN_QUARTER", "FIN_YEAR"]
    predwins={
                # predict 1,2,3,6,9 months
                "FIN_DAY": [24,48,72,144,216],
                "FIN_WEEK": [4,8,12,24,36],
                "FIN_MONTH": [1,2,3,4,5,6,9],
                "FIN_QUARTER": [1,2,3]
            }
    tried=[]
    try:
        with open("experimentresults.txt", "r") as myfile:
            next(myfile)
            for line in myfile:
                time,div,predwin,k,l,mape,mae,rmse = line.rstrip().split(",")
                print("Already trained {}-{}-{} with k={}, l={}, mape={}, mae={}, mse={}".format(time,div,predwin,k,l,mape,mae,rmse))
                tried.append((time,div,int(predwin)))
    except IOError:
        with open("experimentresults.txt", "a") as myfile:
            myfile.write("time,div,predwin,k,l,mape,mae,rmse\n")
    model = KnnTSPI()
    for time in predwins:
        for div in keys_divs:
            t, data = plotdata["{}-{}".format(time,div)]
            data = np.array(data)
            for predwin in predwins[time]:
                if (time,div,predwin) in tried:
                    continue
                train,validation = split_data(data, testcount=predwin)
                newtrain,test = split_data(train, testcount=predwin)
                assert(len(test) == len(validation))
                mape, mae, rmse = model.fit(newtrain,test)
                import measures
                prediction = model.predict(train, timesteps=predwin)
                mape = measures.MAPE(validation, prediction)
                mae = measures.MAE(validation, prediction)
                rmse = measures.RMSE(validation, prediction)
                with open("experimentresults.txt", "a") as myfile:
                    result = "{},{},{},{},{},{},{},{}\n".format(time,div,predwin,model.k,model.l,mape,mae,rmse)
                    myfile.write(result)


    # TODO for each aggregation
