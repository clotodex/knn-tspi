from dataloader import load_data
from pyramid.arima import auto_arima, ARIMA
from knntspi import KnnTSPI
import numpy as np
import measures
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import seaborn as sns

sns.set(style="whitegrid")
#sns.set(style="ticks")
sns.set_context("talk")
sns.set_palette("muted")

#Weekly
#All
#3,6,9
#sarima and knn
#also full plot mit structuralbreak


def split_data(data, testcount=None):
    if testcount:
        return data[:len(data) - testcount], data[len(data) - testcount:]
    return data[:int(len(data)*0.7)], data[int(len(data)*0.7):]

keys_time, keys_divs, plotdata = load_data()

val_time=["FIN_DAY", "FIN_WEEK", "FIN_MONTH", "FIN_QUARTER", "FIN_YEAR"]
predwins={
            "FIN_WEEK": [12,24,36]
        }

USE_TRAINED=True

trained_knn={
    12: (2,14),
    24: (2,39),
    36: (2,35),
}

trained_sarima={
    12: ((2,1,1),(1,1,0,52)),
    24: ((2,1,1),(1,1,0,52)),
    36: ((2,1,1),(1,1,0,52)),
}

t, data = plotdata["FIN_WEEK-ALL"]
data = np.array(data)
lastwn = 100
year = 2014
for i,wn in enumerate(t):
    wn = int(wn)
    if wn < lastwn:
        year += 1
    t[i] = "{} W{}".format(year,wn)
    lastwn = wn

for predwin in predwins["FIN_WEEK"]:
    train, test = split_data(data, predwin)

    train,test = split_data(data, testcount=predwin)
    newtrain,holdout = split_data(train, testcount=predwin)
    assert(len(holdout) == len(test))

    k,l = trained_knn[predwin]
    model = KnnTSPI(k=k,l=l)
    if not USE_TRAINED:
        model.fit(train,test)
    knn_pred = model.predict(train, timesteps=predwin)

    print("knn MAPE: ",  measures.MAPE(test,knn_pred))
    print("knn MAE: ",   measures.MAE (test,knn_pred))
    print("knn RMSE: ",  measures.RMSE(test,knn_pred))
    print("knn MAEP: ",  measures.MAE (test,knn_pred) / np.mean(test) *100)
    print("knn RMSEP: ", measures.RMSE(test,knn_pred) / np.mean(test) *100)

    if USE_TRAINED:
        order,seasonal_order = trained_sarima[predwin]
        #TODO determine trend
        sarima_model = ARIMA(order=order, seasonal_order=seasonal_order)
        sarima_model.fit(train)
    else:
        sarima_model = auto_arima(train, start_p=1, start_q=1,
                                   max_p=8, max_q=8, m=52,
                                   start_P=0, start_Q=0, max_P=8, max_Q=8, seasonal=True,
                                   d=None, D=None, trace=True,
                                   error_action='warn',
                                   suppress_warnings=True,
                                   stepwise=True,random=False, random_state=20, n_fits=50)
    sarima_pred = sarima_model.predict(n_periods=len(test))
    print("fitted arima: {}x{}".format(sarima_model.get_params()['order'], sarima_model.get_params()['seasonal_order']))

    print("sarima MAPE: ",  measures.MAPE(test,sarima_pred))
    print("sarima MAE: ",   measures.MAE (test,sarima_pred))
    print("sarima RMSE: ",  measures.RMSE(test,sarima_pred))
    print("sarima MAEP: ",  measures.MAE (test,sarima_pred) / np.mean(test) *100)
    print("sarima RMSEP: ", measures.RMSE(test,sarima_pred) / np.mean(test) *100)

    # create figure
    f, ax = plt.subplots(1, figsize=(23,10.8), dpi=100)

    # plot data
    p = ax.plot(t[-len(test):], test, label='Actual')
    ax.plot(t[-len(test):], knn_pred, label="kNN-TSPI")
    ax.plot(t[-len(test):], sarima_pred, label="SARIMA")

    # rotate x labels
    plt.xticks(rotation=-45, ha='left', rotation_mode="anchor")

    # Remove ugly 1e7 factor
    ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: ('%.1f')%(x/1e6)))

    # normalize scaling
    ax.set_ylim((-10*1e6,80*1e6))

    # axis labels
    ax.set_ylabel('Revenue (Million €)')
    ax.set_xlabel('Time (Weeks)')

    # remove box around plot - only keep left and bottom ax
    sns.despine()

    # create legend
    ax.legend(loc="lower left")

    # set title
    plt.title("Prediction")

    # display the plot interactively
    #plt.tight_layout()
    import os
    f.savefig(os.path.join('plots','predict_FIN_WEEK-ALL_{}-months_both.png'.format(predwin/4)), bbox_inches='tight')
    #plt.gcf().subplots_adjust(bottom=0.15)
    #plt.show(f)
