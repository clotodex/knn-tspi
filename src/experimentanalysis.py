import pandas as pd
import numpy as np


df = pd.read_csv("experimentresults.txt")
print(df)

#General problem: 5 elements with mape=mae=rmse=0, because their calculation was interrupted

df = df.drop([12,13,14,15])

#rmse = df["rmse"]
rmse_sort = df["rmse"].sort_values(ascending=True)
print(rmse_sort[0:10])
#Best RMSE: Index 74, 26, 27, 31, 28, 32, 81


mape_sort=df["mape"].sort_values(ascending=True)
print(mape_sort[0:10])
#Best MAPE: Index 74, 81, 103, 95, 94, 82, 97


mae_sort = df["mae"].sort_values(ascending=True)
print(mae_sort[0:10])
#Best MAE: 74, 26, 31, 28, 27, 32, 10

print(df.iloc[[74]])
# Disadvantage: Only one month prediction

# TODO: Search best predictor for all three month forecasts (FIN_DAY = 72, FIN_WEEK = 12, FIN_MONTH = 3)