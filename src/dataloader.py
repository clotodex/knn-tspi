from openpyxl import load_workbook
import numpy as np

def load_data(structuralbreak=False, with_columns=False):
    wb = load_workbook('Anonymized sample data.xlsx')#, read_only=True)
    print(wb.sheetnames)

    ws = wb.active

    columns={}

    for col in ws.iter_cols(min_row=3, values_only=True):
        columns[col[0]] = col[1:]

    keys =  [key for key in columns]
    keys_time = keys[:5]
    keys_divs = keys[5:]

    newdiv='ALL'
    columns[newdiv] = []
    total = len(columns[keys_divs[0]])
    for i in range(0,total):
        columns[newdiv].append(sum((columns[div][i] for div in keys_divs)))
    keys_divs += [newdiv]

    print("Keys: ",keys)

    if structuralbreak:
        print("Applying structuralbreak")
        from structural_break import make_structural_break
        time_in_months = 6
        amountpoints = time_in_months*6*4
        for div in keys_divs:
            columns[div] = make_structural_break(np.array(columns[div]), amountpoints=amountpoints)#*2, positionfraction=0.8) #add this when doing a late str brk

    def accumulate(time_col, division_col):
        result_time = []
        result_div = []
        last_time = None
        cur_idx = -1
        for i, time in enumerate(columns[time_col]):
            if time is not None and time is not last_time:
                cur_idx += 1
                last_time = time
                result_time.append(time)
                result_div.append(0)
            result_div[cur_idx] += columns[division_col][i]
        return result_time, result_div

    plotdata = {}
    for time in keys_time:
        for div in keys_divs:
            plotdata["{}-{}".format(time,div)] = accumulate(time,div)

    if with_columns:
        return keys_time, keys_divs, plotdata, columns

    return keys_time, keys_divs, plotdata
