import numpy as np
import measures

time_key = "FIN_MONTH"

if __name__== "__main__":

    def load_data():
        from dataloader import load_data
        keys_time, keys_divs, plotdata = load_data(structuralbreak=False)
        time, points = np.array(plotdata["{}-ALL".format(time_key)])
        return np.array(points, dtype=np.float64)

    def split_data(data, testcount=None):
        if testcount:
            return data[:len(data) - testcount], data[len(data) - testcount:]
        return data[:int(len(data)*0.7)], data[int(len(data)*0.7):]


    data = load_data()#[:-1]
    predwins = {
            "FIN_WEEK": [4,8,12,24,36],
            "FIN_MONTH": [1,2,3,4,5,6,7,8,9],
    }
    div = "ALL"

    for predwin in predwins[time_key]:
        train,test = split_data(data, testcount=predwin)
        tm = test.mean()

        x = train.mean()
        mape = measures.MAPE(test, x)
        mpe = measures.MPE(test, x)
        rmse = measures.RMSE(test, x)
        mae = measures.MAE(test, x)
        ep_rmse = measures.RMSE(test, x)/tm * 100
        ep_mae = measures.MAE(test, x)/tm * 100

        with open("mean_predictor_experimentresults_ep.txt", "a") as myfile:
            result = "{},{},{},{},{},{},{},{}\n".format(time_key,div,predwin,mape,mae,rmse,ep_mae,ep_rmse)
            myfile.write(result)

        y = train[-1]
        mape = measures.MAPE(test, y)
        mpe = measures.MPE(test, y)
        rmse = measures.RMSE(test, y)
        mae = measures.MAE(test, y)
        ep_rmse = measures.RMSE(test, y)/tm *100
        ep_mae = measures.MAE(test, y)/tm *100

        with open("lastpoint_predictor_experimentresults_ep.txt", "a") as myfile:
            result = "{},{},{},{},{},{},{},{}\n".format(time_key,div,predwin,mape,mae,rmse,ep_mae,ep_rmse)
            myfile.write(result)
