import numpy as np

def MAPE(y_true, y_pred):
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

# trendprediction - aber antitrend wäre perfekter MPE!!!
def MPE(y_true, y_pred):
    return np.mean((y_true - y_pred) / y_true) * 100

def MSE(y_true, y_pred):
    return ((y_true - y_pred)**2).mean()

def RMSE(y_true, y_pred):
    return np.sqrt(MSE(y_true, y_pred))

def MAE(y_true, y_pred):
    return np.abs((y_true - y_pred)).mean()
