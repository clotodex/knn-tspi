from dataloader import load_data
import measures
from knntspi import KnnTSPI
import numpy as np

if __name__== "__main__":
    def split_data(data, testcount=None):
        if testcount:
            return data[:len(data) - testcount], data[len(data) - testcount:]
        return data[:int(len(data)*0.7)], data[int(len(data)*0.7):]

    keys_time, keys_divs, plotdata = load_data()

    val_time=["FIN_DAY", "FIN_WEEK", "FIN_MONTH", "FIN_QUARTER", "FIN_YEAR"]
    predwins={
                # predict 1,2,3,6,9 months
                "FIN_DAY": [24,48,72,144,216],
                "FIN_WEEK": [4,8,12,24,36],
                "FIN_MONTH": [1,2,3,4,5,6,9],
                "FIN_QUARTER": [1,2,3]
            }
    tried=[]
    try:
        with open("general_experimentresults.txt", "r") as myfile:
            next(myfile)
            for line in myfile:
                time,div,predwin,k,l,mape,mae,rmse = line.rstrip().split(",")
                print("Already trained {}-{}-{} with k={}, l={}, mape={}, mae={}, mse={}".format(time,div,k,l,mape,mae,rmse))
                tried.append((time,div))
    except IOError:
        with open("general_experimentresults.txt", "a") as myfile:
            myfile.write("time,div,predwin,k,l,mape,mae,rmse\n")
    model = KnnTSPI()
    for time in predwins:
        for div in keys_divs:
            if (time,div) in tried:
                continue
            t, data = plotdata["{}-{}".format(time,div)]
            data = np.array(data)
            # [-----][---][---]
            longest_pred = predwins[-1]
            train,validation = split_data(data, testcount=longest_pred)
            newtrain,test = split_data(train, testcount=longest_pred)

            allresults = {}
            for predwin in predwins[time]:
                result = model.fit(newtrain,test[:predwin],retresult=True)
                for mape, mae, rmse, l, k in result:
                    if (k,l) not in allresults:
                        allresults[(k,l)] = (0,0,0)
                    allresults[(k,l)][0] += mape
                    allresults[(k,l)][1] += mae
                    allresults[(k,l)][2] += rmse

            print("done training")
            sorted_mape = sorted(allresults.items(), key=lambda kv: kv[1][0])
            sorted_mae = sorted(allresults.items(), key=lambda kv: kv[1][1])
            sorted_rmse = sorted(allresults.items(), key=lambda kv: kv[1][2])

            (k,l),(mape,_,_) = sorted_mape[0]
            print("Achieved MAPE: {} (k={}, l={})".format(mape,k,l))
            (k,l),(_,mae,_) = sorted_mae[0]
            print("Achieved MAE: {} (k={}, l={})".format(mae,k,l))
            (k,l),(_,_,rmse) = sorted_rmse[0]
            print("Achieved RMSE: {} (k={}, l={})".format(mse,k,l))

            model = KnnTSPI(k=k, l=l)
            for predwin in predwins[time]:
                train,validation = split_data(train, testcount=predwin)
                prediction = model.predict(train, timesteps=predwin)
                mape = measures.MAPE(validation, prediction)
                mae = measures.MAE(validation, prediction)
                rmse = measures.RMSE(validation, prediction)
                with open("general_experimentresults.txt", "a") as myfile:
                    result = "{},{},{},{},{},{},{},{}\n".format(time,div,predwin,k,l,mape,mae,rmse)
                    myfile.write(result)
