from dataloader import load_data
import matplotlib.pyplot as plt
from knntspi import KnnTSPI
import numpy as np
from tqdm import tqdm
import measures

#FIXME problem: weeks are rightshifted since we have nnot enough predictions
# predictions start left, accumulation right => error
def accumulate_steps(base_values, acc_values, base_time, acc_time, columns,div):
    # go from back stepwise further and further and then substract from each other
    new_base_values = []
    new_acc_values = []
    for steplength in range(1,len(acc_values)+1):
        print("steplength: ", steplength)
        print("accvals: ",acc_values)
        tmp_acc_values = acc_values[-steplength:]
        print("tmp_acc_values: ",tmp_acc_values)
        base_sum, acc_sum = accumulate(base_values, tmp_acc_values, base_time, acc_time, columns,div, intermediate_prediction=(len(acc_values) != steplength))
        new_base_values.append(base_sum - sum(new_base_values))
        new_acc_values.append(acc_sum - sum(new_acc_values))
    return np.array(new_base_values[::-1]), np.array(new_acc_values[::-1])


def accumulate(base_values, acc_values, base_time, acc_time, columns,div, intermediate_prediction=False):
    pred = sum(base_values)
#    val = sum(acc_values)

    last_base_idx = None
    last_acc_idx = None

    last_base_time = None
    last_acc_time = None
    acc_count = 0
    base_count = 0
    accdone = False
    basedone = False
    for i in reversed(range(len(columns[base_time]))):
        baset = columns[base_time][i]
        acct = columns[acc_time][i]
        if baset is not None and baset is not last_base_time and not basedone:
            base_count += 1
            last_base_time = baset
            last_base_idx = i
            if intermediate_prediction and accdone:
                last_base_idx += 1
                basedone = True
                break
            if not intermediate_prediction and base_count == len(base_values)+1:
                last_base_idx += 1
                basedone = True
                print("done base")

        if acct is not None and acct is not last_acc_time and not accdone:
            last_acc_time = acct
            last_acc_idx = i
            acc_count += 1
            if acc_count == len(acc_values):
                accdone = True
                print("done acc")
        if accdone and basedone:
            break
    if not accdone:
        print("loop ran out")
        last_acc_idx = 0
    if not basedone:
        print("loop ran out")
        last_base_idx = 0
    print("base len: ", len(base_values))
    print("base count: ", base_count)
    print("last base i: ", last_base_idx)
    print("last acc i: ", last_acc_idx)

    if last_base_idx < last_acc_idx:
        # more week data than month data
        # FIXME this will never happen since overflow failes!
        print("range overflow: {} - {}".format(last_base_idx, last_acc_idx))
        print("range: ", [range(last_base_idx, last_acc_idx)])
        correction = sum(columns[div][last_base_idx:last_acc_idx])
        return sum(base_values) - correction, sum(acc_values)
    elif last_base_idx > last_acc_idx:
        # more week data than month data
        print("range underflow: {} - {}".format(last_base_idx, last_acc_idx)) # excluding left range
        print("range: ", [range(last_acc_idx, last_base_idx)])
        correction = sum(columns[div][last_acc_idx:last_base_idx])
        return sum(base_values), sum(acc_values)-correction
    else:
        print("All fine!")
        return sum(base_values), sum(acc_values)
#def accumulate_steps(base_values, acc_values, base_time, acc_time, columns,div):
#    # go from back stepwise further and further and then substract from each other
#    new_base_values = []
#    new_acc_values = []
#    for steplength in range(1,len(acc_values)+1):
#        tmp_acc_values = acc_values[-steplength:]
#        base_sum, acc_sum = accumulate(base_values, tmp_acc_values, base_time, acc_time, columns,div, intermediate_prediction=(len(acc_values) != steplength))
#        new_base_values.append(base_sum - sum(new_base_values))
#        new_acc_values.append(acc_sum - sum(new_acc_values))
#    return np.array(new_base_values[::-1]), np.array(new_acc_values[::-1])
#
#
#def accumulate(base_values, acc_values, base_time, acc_time, columns,div, intermediate_prediction=False):
#    pred = sum(base_values)
##    val = sum(acc_values)
#
#    last_base_idx = None
#    last_acc_idx = None
#
#    last_base_time = None
#    last_acc_time = None
#    acc_count = 0
#    base_count = 0
#    accdone = False
#    basedone = False
#    for i in reversed(range(len(columns[base_time]))):
#        baset = columns[base_time][i]
#        acct = columns[acc_time][i]
#        if baset is not None and baset is not last_base_time and not basedone:
#            base_count += 1
#            last_base_time = baset
#            last_base_idx = i
#            if intermediate_prediction and accdone:
#                last_base_idx += 1
#                basedone = True
#                break
#            if not intermediate_prediction and base_count == len(base_values)+1:
#                last_base_idx += 1
#                basedone = True
#
#        if acct is not None and acct is not last_acc_time and not accdone:
#            last_acc_time = acct
#            last_acc_idx = i
#            acc_count += 1
#            if acc_count == len(acc_values):
#                accdone = True
#        if accdone and basedone:
#            break
#    if not accdone:
#        last_acc_idx = 0
#    if not basedone:
#        last_base_idx = 0
#
#    if last_base_idx < last_acc_idx:
#        # more week data than month data
#        correction = sum(columns[div][last_base_idx:last_acc_idx])
#        return sum(base_values) - correction, sum(acc_values)
#    elif last_base_idx > last_acc_idx:
#        # more week data than month data
#        correction = sum(columns[div][last_acc_idx:last_base_idx])
#        return sum(base_values), sum(acc_values)-correction
#    else:
#        print("All fine!")
#        return sum(base_values), sum(acc_values)
if __name__== "__main__":
    def split_data(data, testcount=None):
        if testcount:
            return data[:len(data) - testcount], data[len(data) - testcount:]
        return data[:int(len(data)*0.7)], data[int(len(data)*0.7):]

    keys_time, keys_divs, plotdata, columns = load_data(with_columns=True)

    val_time=["FIN_DAY", "FIN_WEEK", "FIN_MONTH", "FIN_QUARTER", "FIN_YEAR"]
    predwins={
                # predict 1,2,3,6,9 months
                "FIN_DAY": [24,72,144,216],
                "FIN_WEEK": [4,12,24,36],
                "FIN_MONTH": [1,3,6,9],
                "FIN_QUARTER": [1,2,3]
            }
    trained={}
    try:
        with open("experimentresults.txt", "r") as myfile:
            next(myfile)
            for line in myfile:
                time,div,predwin,k,l,mape,mae,rmse = line.rstrip().split(",")
                trained[(time,div,int(predwin))] = (int(k), int(l))
    except IOError:
        print("no experimentresults.txt found")
        exit(1)

    # TODO structural breaks

    validations = [(base_time, acc_time, div, predwin_ind) for base_time in val_time[1:2] for acc_time in val_time[2:3] for div in keys_divs for predwin_ind in range(len(predwins[base_time]))]
    for base_time, acc_time, div, predwin_ind in tqdm(validations):
        base_predwin = predwins[base_time][predwin_ind]
        acc_predwin = predwins[acc_time][predwin_ind]
        tqdm.write("Comparing {}-prediction to {}-validation for {} {}s on {} division".format(base_time, acc_time, acc_predwin, acc_time, div))

        k,l = trained[(base_time, div, base_predwin)]
        model = KnnTSPI(k=k,l=l)

        base_t, base_data = plotdata["{}-{}".format(base_time, div)]
        acc_t, acc_data = plotdata["{}-{}".format(acc_time, div)]
        base_data = np.array(base_data)
        acc_data = np.array(acc_data)

        base_train, base_test = split_data(base_data, base_predwin)
        acc_train, acc_test = split_data(acc_data, acc_predwin)

        base_pred = model.predict(base_data, timesteps=base_predwin)

        new_base_values, new_acc_values = accumulate_steps(base_pred, acc_test, base_time, acc_time, columns,div)

        print("acc MAPE: ", measures.MAPE(new_base_values, new_acc_values))
        print("acc MAE: ", measures.MAE(new_base_values, new_acc_values))
        print("acc RMSE: ", measures.RMSE(new_base_values, new_acc_values))
        print("acc MAEP: ", measures.MAE(new_base_values, new_acc_values) / np.mean(new_acc_values) *100)
        print("acc RMSEP: ", measures.RMSE(new_base_values, new_acc_values) / np.mean(new_acc_values) *100)
        #assert(int(sum(new_base_values)) == int(sum(new_acc_values)))
        k,l = trained[(acc_time, div, acc_predwin)]
        model = KnnTSPI(k=k,l=l)
        acc_pred = model.predict(acc_data, timesteps=acc_predwin)
        print("test MAPE: ",  measures.MAPE(acc_pred, acc_test))
        print("test MAE: ",   measures.MAE (acc_pred, acc_test))
        print("test RMSE: ",  measures.RMSE(acc_pred, acc_test))
        print("test MAEP: ",  measures.MAE (acc_pred, acc_test) / np.mean(new_acc_values) *100)
        print("test RMSEP: ", measures.RMSE(acc_pred, acc_test) / np.mean(new_acc_values) *100)

        f, ax = plt.subplots(1)
        PREDICTIONS = len(acc_test)
        ax.plot(range(PREDICTIONS), new_base_values, label="accumulated prediction")
        ax.plot(range(PREDICTIONS), new_acc_values, label="modified actual")
        ax.plot(range(PREDICTIONS), acc_pred, label="normal prediction")
        ax.plot(range(PREDICTIONS), acc_test, label="actual")
        #ax.set_ylim(ymin=0)
        ax.legend()
        plt.title("Comparing {}-prediction to {}-validation for {} {}s on {} division".format(base_time, acc_time, acc_predwin, acc_time, div))
        plt.show(f)
