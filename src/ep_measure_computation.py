import sys
import os
cwd = os.getcwd()
sys.path.insert(0,os.path.join(cwd,'src'))

from dataloader import load_data
import numpy as np

if __name__== "__main__":
    def split_data(data, testcount=None):
        if testcount:
            return data[:len(data) - testcount], data[len(data) - testcount:]
        return data[:int(len(data)*0.7)], data[int(len(data)*0.7):]

    keys_time, keys_divs, plotdata = load_data()

    results=[]
    try:
        with open("validation_experimentresults.txt", "r") as myfile:
            next(myfile)
            for line in myfile:
                time,div,predwin,k,l,mape,mae,rmse = line.rstrip().split(",")
                results.append((time,div,int(predwin),k,l,float(mape),float(mae),float(rmse)))
    except IOError:
        exit()

    with open('validation_experimentresults_ep.txt','w') as file:
        file.write('time,div,predwin,k,l,mape,mae,rmse,ep_mae,ep_rmse\n')
        for time,div,predwin,k,l,mape,mae,rmse in results:
            t, data = plotdata["{}-{}".format(time,div)]
            data = np.array(data)
            train,test = split_data(data, testcount=predwin)
            mean = np.mean(test)
            ep_mae = mae/mean *100
            ep_rmse = rmse/mean *100
            result = "{},{},{},{},{},{},{},{},{},{}\n".format(time,div,predwin,k,l,mape,mae,rmse,ep_mae,ep_rmse)
            file.write(result)
