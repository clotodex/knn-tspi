from dataloader import load_data
from knntspi import KnnTSPI
import numpy as np
import measures
from tqdm import tqdm

if __name__== "__main__":
    def split_data(data, testcount=None):
        if testcount:
            return data[:len(data) - testcount], data[len(data) - testcount:]
        return data[:int(len(data)*0.7)], data[int(len(data)*0.7):]

    keys_time, keys_divs, plotdata = load_data()

    val_time=["FIN_DAY", "FIN_WEEK", "FIN_MONTH", "FIN_QUARTER", "FIN_YEAR"]
    predwins={
                # predict 1,2,3,6,9 months
                "FIN_DAY": [24,72,144,216], # exclude 48
                "FIN_WEEK": [4,8,12,24,36],
                "FIN_MONTH": [1,2,3,4,5,6,9],
                "FIN_QUARTER": [1,2,3]
            }
    frequency={
                "FIN_DAY": 7,
                "FIN_WEEK": 52,
                "FIN_MONTH": 12,
                "FIN_QUARTER": 4,
    }

    tried=[]
    try:
        with open("arima_experimentresults.txt", "r") as myfile:
            next(myfile)
            for line in myfile:
                time,div,predwin,p,d,q,P,D,Q,m,mape,mae,rmse = line.rstrip().split(",")
                print("Already trained {}-{}-{} with ({},{},{})x({},{},{},{}), mape={}, mae={}, mse={}".format(time,div,predwin,p,d,q,P,D,Q,m,mape,mae,rmse))
                tried.append((time,div,int(predwin)))
    except IOError:
        with open("arima_experimentresults.txt", "a") as myfile:
            myfile.write("time,div,predwin,orderp,orderd,orderq,seasonalp,seaonald,seasonalq,m,mape,mae,rmse\n")

    configurations = [(time,div,predwin) for time in predwins for div in keys_divs for predwin in predwins[time] if (time,div,predwin) not in tried]
    for (time,div,predwin) in tqdm(configurations):
        t, data = plotdata["{}-{}".format(time,div)]
        data = np.array(data)
        train,test = split_data(data, testcount=predwin)

        m = frequency[time]
        from pyramid.arima import auto_arima
        stepwise_model = auto_arima(train, start_p=1, start_q=1,
                                   max_p=8, max_q=8, m=m,
                                   start_P=0, start_Q=0, max_P=8, max_Q=8, seasonal=True,
                                   d=None, D=None, trace=False,
                                   error_action='ignore',
                                   suppress_warnings=True,
                                   stepwise=True,random=False, random_state=20, n_fits=50)
        future_forecast = stepwise_model.predict(n_periods=len(test))

        mape, mae, rmse = measures.MAPE(test,future_forecast), measures.MAE(test,future_forecast), measures.RMSE(test,future_forecast)

        params = stepwise_model.get_params()
        order = params['order']
        seasonal = params['seasonal_order']

        with open("arima_experimentresults.txt", "a") as myfile:
            result = "{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format(time,div,predwin,*order,*seasonal,mape,mae,rmse)
            myfile.write(result)
