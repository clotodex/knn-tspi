##
# Source code for the kNN-TSPI algorithm
#

import knn_util
import numpy as np
import math
import measures
from multiprocessing import Pool  # thread pool
import multiprocessing as mp
from tqdm import tqdm  # simple progress report

from functools import partial

def fit_one(lk, train, test, h_tick):
    l,k = lk
    pred = knn_util.prediction_alg(train, l, k, h_tick)
    mape = measures.MAPE(test[:h_tick],pred)
    mae = measures.MAE(test[:h_tick],pred)
    mse = measures.RMSE(test[:h_tick],pred)
    return mape,mae,mse,l,k

class KnnTSPI:
    def __init__(self, k=None, l=None):
        self.k = k
        self.l = l

    def fit(self, train, test, prefix="", retresults=False):
        # TODO fit k and l to perform best on test set
        # input train, test, maxp, h

        max_p = len(train)//3
        max_k = max(5, min(50, len(train)//5))
        h = max_p

        # h 0 = (max p + h) ÷ 2
        h_tick = (max_p + h) // 2
        h_tick = len(test)

        to_try = [(l,k) for l in range(2,max_p) for k in range(1,max_k)]

        results=[]

        pool = Pool(mp.cpu_count())  # (n) number of threads equal to number of CPUs
        it = pool.imap_unordered(partial(fit_one, train=train, test=test, h_tick=h_tick), to_try)
        for mape,mae,mse,l,k in tqdm(it, total=len(to_try), position=0):
            results.append((mape,mae,mse, l, k))
            #tqdm.write("new error l={} k={} mape={}, mae={}, mse={}".format(l,k,mape,mae,mse))
        pool.close()
        pool.join()

        if retresults:
            return results

        print("done training")
        results.sort(key=lambda x: x[0])
        mape,_,_, l_best, k_best = results[0]
        print("Achieved MAPE: {} (k={}, l={})".format(mape,k_best,l_best))
        results.sort(key=lambda x: x[1])
        _,mae,_, l_best, k_best = results[0]
        print("Achieved MAE: {} (k={}, l={})".format(mae,k_best,l_best))
        results.sort(key=lambda x: x[2])
        _,_,mse, l_best, k_best = results[0]
        print("Achieved RMSE: {} (k={}, l={})".format(mse,k_best,l_best))

        self.k = k_best
        self.l = l_best
        print("best l: ",l_best)
        print("best k: ",k_best)

        # dumping results
        np.savetxt("results_{}_{}_{}.csv".format(prefix, max_k, max_p), np.array(results), fmt="%d", delimiter=",")
        return mape,mae,mse

    def predict(self, trainingdata, timesteps=1):
        print("Predict with: data: {}, timesteps: {}, k: {}, l: {}".format(trainingdata.shape, timesteps, self.k, self.l))
        pred = knn_util.prediction_alg(trainingdata, self.l, self.k, timesteps)
        return pred
