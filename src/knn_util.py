import numpy as np
import multiprocessing as mp
import math
from tqdm import tqdm

def generate_subsequences(data,l):
    J = np.arange(0, len(data) - 2*l + 1)
    S = np.empty(shape=(len(J),l+1))
    for j in J:
        S[j] = data[j:j+l+1]
    return J, S

def z_scores_seq(C):
    mean = np.mean(C)
    std = np.std(C)
    std = 1 if std == 0 else std
    return (C - mean) / std

def z_scores(S):
    for i,ss in enumerate(S):
        S[i,::] = z_scores_seq(ss)
    return S

def EQ(Q, C):
    return np.linalg.norm(Q-C)

## returns a number
def CE(Q):
    #sqrt(sum(q - q+1)^2)
    Q_left = np.insert(Q,0,0)
    Q_right = np.append(Q,0)

    diff = Q_left - Q_right
    diff = diff[1:-1]
    sq = diff**2
    res = math.sqrt(sq.sum())
    return res

def CF(Q, C):
    # epsilon to prevent div by zero
    CE_Q = CE(Q) + np.finfo(float).eps
    CE_C = CE(C) + np.finfo(float).eps
    return max(CE_Q, CE_C) / min(CE_Q, CE_C)

def CID_Seq(Q,C):
    return EQ(Q, C) * CF(Q, C)

def CID(Q, S_tick):
    def cidq(C):
        return CID_Seq(Q,C)
    return np.apply_along_axis(cidq, 1, S_tick)

def search_nearest_neighbors(J,D,k,l):
    # Future improvement - allow partial overlap
    indices = np.argsort(D)

    kind = []
    for i in indices:
        # check if index overlaps with already found sequences
        overlaps=False
        for i_k in kind:
            j_k = J[i_k]
            assert(j_k == i_k)
            j = J[i]
            assert(j == i)
            if abs(j-j_k) <= l:
                overlaps=True
                break
        if overlaps:
            continue
        kind.append(i)
        if (len(kind) == k):
            break
    return kind


def make_query(data, l):
    Q_unnormalized = data[len(data)-l:]
    q_mean = np.mean(Q_unnormalized)
    q_std = np.std(Q_unnormalized)
    q_std = 1 if q_std == 0 else q_std
    Q = (Q_unnormalized - q_mean) / q_std
    return q_mean, q_std, Q

def prediction_alg(data, l, k, timesteps):
    # Z represents a TS with m observations
    # l is the query length in number of observations
    # k indicates the number of similar subsequences

    datalen = len(data)

    # S(j) 1..l contains a subsequence of length l which begins in observation j of the TS
    # generate l+1 value for later
    J,S = generate_subsequences(data, l) # S = list of j's, maybe all of them?

    q_mean, q_std, Q = make_query(data, l)

    # S (j) is the z-normalization of subsequence S (j)
    # TODO separately compute normalization of next value!
    S_tick = z_scores(S)
    # extract l+1 values
    R = S_tick[::,-1]
    S_tick = S_tick[::,:-1]

    try:
        worker_id=int(mp.current_process().name.split("-")[1])
        worker_id = worker_id % mp.cpu_count() + 1
        kwargs = { 'position' : worker_id }
    except:
        kwargs={}
    for i in tqdm(range(timesteps), leave=True, **kwargs):
        # D (j) contains the complexity-invariant distance between Q and S (j) , for 1 ≤ j ≤ m-l+1
        D = CID(Q, S_tick)

        # Choosing of the k most similar subsequences
        kind = search_nearest_neighbors(J, D, k, l);
        # Obtaining the next value of each of the k most similar subsequences ∈ P, where S l+1 indicates the next z-normalized value
        R_tick = R[kind]

        # Mapping of z-normalization to query values and calculation of prediction
        Z = q_std * R_tick + q_mean
        z = sum(Z) / float(len(Z))

        # PREDICTION DONE

        data = np.append(data, z)

        # new j
        new_j = J[-1] + 1
        J = np.append(J, new_j)

        # make new query
        q_mean, q_std, Q = make_query(data, l)

        # append new normalized sequence
        new_s_tick = z_scores_seq(data[new_j:new_j+l+1])
        new_r = new_s_tick[-1]
        new_s_tick = new_s_tick[:-1]
        S_tick = np.vstack([S_tick, new_s_tick])

        # and its normalized next value
        R = np.append(R, new_r)

    return data[datalen:]
