import numpy as np

def make_structural_break(datapoints, amountpoints=5, positionfraction=0.2, factor=0.4):
    return make_structural_break_normalized(datapoints, amountpoints, positionfraction, factor)

def make_structural_break_normalized(datapoints, amountpoints=5, positionfraction=0.2, factor=0.4):
    start_ind = int(len(datapoints) * positionfraction)
    end_ind = start_ind + amountpoints

    datapoints[start_ind:end_ind] = factor * datapoints[start_ind:end_ind]

    mean = np.mean(datapoints[start_ind:end_ind])
    std = np.std(datapoints[start_ind:end_ind])

    meantrans = (datapoints[start_ind:end_ind] - mean)
    noramlized = (meantrans/std)

    newmean = factor * mean
    newstd = factor * std

    datapoints[start_ind:end_ind] = (noramlized * newstd) + newmean

    # add random noise
    return datapoints

def make_structural_break_factor(datapoints, amountpoints=5, positionfraction=0.2, factor=0.4):
    start_ind = int(len(datapoints) * positionfraction)
    end_ind = start_ind + amountpoints

    datapoints[start_ind:end_ind] = factor * datapoints[start_ind:end_ind]

    # add random noise
    return datapoints

def make_structural_break_offset(datapoints, amountpoints=5, positionfraction=0.2, factor=0.6):
    start_ind = int(len(datapoints) * positionfraction)
    end_ind = start_ind + amountpoints

    mean = sum(datapoints[start_ind:end_ind]) / len(datapoints[start_ind:end_ind])
    offset = (1-factor) * mean
    datapoints[start_ind:end_ind] = datapoints[start_ind:end_ind] - offset

    # add random noise
    return datapoints
