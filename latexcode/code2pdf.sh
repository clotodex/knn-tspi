#!/bin/bash

output="pdfs"
mkdir -p "$output"

for filename in ../src/*.py; do
	echo "### FILE: $filename"
	if [ ! -f "$filename" ]; then
		echo "$filename exist and it is a directory"
		continue
	fi
	#filename="../src/knntspi.py"
	filebase="$(basename "$filename")"
	sed -e "s%\\\$FILENAME\\\$%${filename}%g" doc.tex > tmp.tex
	xelatex --shell-escape tmp.tex
	#convert -density 300 tmp.pdf "$output/$filebase.jpg"
    convert -density 600 tmp.pdf "$output/$filebase-%01d.jpg"
	mv tmp.pdf "$output/$filebase.pdf"
	rm tmp*
done
